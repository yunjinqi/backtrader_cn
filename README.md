# backtrader_cn

#### 对这个包将不再维护，而是把backtrader和pyfolio这两个包，分开，这样方便后续的维护。

***backtrader：https://gitee.com/yunjinqi/backtrader***

***pyfolio:https://gitee.com/yunjinqi/pyfolio***





#### 介绍
backtrader和pyfolio的版本集合，对pyfolio进行了部分改动，对backtrader进行了注释和部分改动.

#### 软件架构
软件架构说明


#### 安装教程

1. 使用git或者手动下载到本地,把backtrader和pyfolio复制到python模块所在的位置

   如果使用的是集成环境anaconda
    - 在windows中,应该把pyfolio的代码复制到anaconda/lib/site-packages/中,
    - 在ubuntu中,应该把pyfolio的代码放到anaconda3/lib/python3.x/site-packages/中.

#### 使用说明

注:使用中,如果出现matplotlib的问题,可能是因为版本问题,卸载matplotlib,然后安装一个旧的版本.

```
pip uninstall matplotlib
pip install matplotlib==3.1.0
```



使用方法可以参考下面的代码
```
import pyfolio as pd
import backtrader as bt

# 省略大部分代码,只保留关键部分

cerebro.addanalyzer(bt.analyzers.PyFolio)
# 运行回测
results = cerebro.run()

pyfoliozer = results[0].analyzers.getbyname('pyfolio')
returns, positions, transactions, gross_lev = pyfoliozer.get_pf_items()
pf.create_full_tear_sheet_by_flask(
    returns,
    positions=positions,
    transactions=transactions,
    # gross_lev=gross_lev,
    live_start_date='2019-01-01',
    run_flask_app = True,# 默认是True,运行之后,会弹出一个网址,打开这个网址,就会显示结果,
                         # 需要使用ctrl + c 等方式取消代码的运行,参数优化中不太适合使用
    )

```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 待完成工作

- [ ]  美化flask显示的页面,希望能够做成类似dash的一个展示结果:https://dash.gallery/dash-financial-report/
- [ ]  整合一些底层数据,使得pyfolio能够更加方便的进行绩效分析,而不仅仅是交易结果的统计
- [ ]  pyfolio画图不够好,后期考虑使用plotly进行改进

